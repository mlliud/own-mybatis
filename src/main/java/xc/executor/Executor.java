package xc.executor;


import xc.pojo.Configuration;
import xc.pojo.MapperStatement;

import java.util.List;

public interface Executor { 

    <E> List<E> query(Configuration configuration, MapperStatement mapperStatement, Object param) throws Exception;

    void close();
}
