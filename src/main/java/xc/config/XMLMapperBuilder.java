package xc.config;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import xc.pojo.Configuration;
import xc.pojo.MapperStatement;

import java.io.InputStream;
import java.util.List;

/**
 * @Description 专门解析映射配置文件的对象
 * @Author xuchang
 * @Date 2022/10/19 10:54:16
 * @Version 1.0
 */
public class XMLMapperBuilder {

    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public void parse(InputStream resourceAsStream) throws Exception {
        // 将输入流转化为Document对象，并获取跟节点<mapper>
        Document document = new SAXReader().read(resourceAsStream);
        Element rootElement = document.getRootElement();

        // 例：<mapper namespace="user">
        String namespace = rootElement.attributeValue("namespace");
        /* 例：
            <select id="selectOne" resultType="com.xc.pojo.User" parameterType="com.xc.pojo.User" >
                select * from user where id = #{id} and name = #{name}
            </select>
        */
        List<Element> selectList = rootElement.selectNodes("//select");
        for (Element element : selectList) {
            String id = element.attributeValue("id");
            String resultType = element.attributeValue("resultType");
            String parameterType = element.attributeValue("parameterType");
            String sql = element.getTextTrim();

            // 封装MapperStatement对象
            MapperStatement mapperStatement = new MapperStatement();
            String statementId = namespace + "." + id;
            mapperStatement.setStatementId(statementId);
            mapperStatement.setParameterType(parameterType);
            mapperStatement.setResultType(resultType);
            mapperStatement.setSql(sql);
            mapperStatement.setSqlCommandType("select");

            // 添加到configurations的map集合中
            configuration.getMapperStatementMap().put(statementId,mapperStatement);
        }
    }
}
