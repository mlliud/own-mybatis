package xc.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import xc.utils.ParameterMapping;

import java.util.List;

/**
 * @Description
 * @Author xuchang
 * @Date 2022/10/19 16:19:32
 * @Version 1.0
 */
@Data
@AllArgsConstructor
public class BoundSql {

    // 用?做占位符的sql语句
    private String finalSql;

    // 字段名称的集合
    private List<ParameterMapping> parameterMappingList;
}
