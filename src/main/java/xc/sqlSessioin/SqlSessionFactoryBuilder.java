package xc.sqlSessioin;

import xc.config.XMLConfigBuilder;
import xc.pojo.Configuration;

import java.io.InputStream;

/**
 * @Description
 * @Author xuchang
 * @Date 2022/10/11 21:39:07
 * @Version 1.0
 */
public class SqlSessionFactoryBuilder {

    /**
     * @Description 1.解析配置文件，封装容器对象 2.创建SqlSessionFactory工厂对象
     * @Author xuchang
     * @Date 2022/10/11 21:42:24
     */
    public SqlSessionFactory build(InputStream inputStream) throws Exception {
        //1.解析配置文件，封装容器对象：专门解析核心配置文件的解析类
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parse(inputStream);
        //2.创建SqlSessionFactory工厂对象
        return new DefaultSqlSessionFactory(configuration);
    }

}
