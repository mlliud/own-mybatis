package xc.sqlSessioin;

/**
 * @Description
 * @Author xuchang
 * @Date 2022/10/11 21:40:51
 * @Version 1.0
 */
public interface SqlSessionFactory {

    // 创建SqlSession对象
    SqlSession openSession();

}
