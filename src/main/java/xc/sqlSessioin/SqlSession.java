package xc.sqlSessioin;

import java.util.List;

/**
 * @Description 1.通过statementId：namespace.id 定位到具体sql语句
 *              2.param参数替换sql语句中的占位符？
 * @Author xuchang
 * @Date 2022/10/19 13:45:00
 * @Version 1.0
 */
public interface SqlSession {

    // 查询多个结果
    <E> List<E> selectList(String statementId, Object param) throws Exception;

    // 查询单个结果
    <T> T selectOne(String statementId, Object param) throws Exception;

    // 清理资源
    void close();

    // 生成代理对象
    <T> T getMapper(Class<?> mapperClass);

}
