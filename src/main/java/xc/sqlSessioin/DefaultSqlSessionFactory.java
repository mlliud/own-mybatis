package xc.sqlSessioin;

import xc.executor.Executor;
import xc.executor.SimpleExecutor;
import xc.pojo.Configuration;


/**
 * @Description
 * @Author xuchang
 * @Date 2022/10/19 13:42:27
 * @Version 1.0
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        // 1.创建执行器对象-具体的包装jdbc的sql操作，关闭连接等
        Executor simpleExecutor = new SimpleExecutor();

        // 2.创建sqlSession对象-判断执行增删改查哪些操作等
        return new DefaultSqlSession(configuration,simpleExecutor);
    }
}
