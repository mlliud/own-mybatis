package xc.pojo;

import lombok.Data;

/**
 * @Description 映射配置类：mapper.xml解析内容
 * @Author xuchang
 * @Date 2022/10/9 23:04:08
 * @Version 1.0
 */
@Data
public class MapperStatement {

    //唯一标识 statementId：namespace.id
    private String statementId;

    //返回值类型
    private String resultType;

    //参数类型
    private String parameterType;

    //sql语句
    private String sql;

    // 判断当前是什么操作的一个属性-增删改查
    private String sqlCommandType;
}
