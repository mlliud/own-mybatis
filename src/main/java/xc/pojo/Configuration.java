package xc.pojo;

import lombok.Data;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 全局配置类：存放核心配置文件解析出来的内容
 * @Author xuchang
 * @Date 2022/10/9 23:05:30
 * @Version 1.0
 */
@Data
public class Configuration {

    //数据源对象
    private DataSource dataSource;

    //map.xml对象集合 key：statementId
    private Map<String,MapperStatement> mapperStatementMap = new HashMap<>();
}
