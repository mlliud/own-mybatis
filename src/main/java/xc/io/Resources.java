package xc.io;

import java.io.InputStream;

/**
 * @Description 读取资源类
 * @Author xuchang
 * @Date 2022/10/9 22:49:07
 * @Version 1.0
 */
public class Resources {
    /**
     * @Description 根据配置文件的路径，加载配置文件成字节输入流，存到内存中
     * @Author xuchang
     * @Date 2022/10/9 22:54:18
     */
    public static InputStream getResourceAsStream(String path){
        return Resources.class.getClassLoader().getResourceAsStream(path);
    }
}
